# SingleStone Cloud Exercises

I really enjoyed completing these exercises! I like the amount of autonomy that was provided in them.

## Set Up

If you wanted to deploy my code into your own AWS account you would need to do the following

- Set up your AWS credentials
    - [AWS documentation for security credentials](https://console.aws.amazon.com/iam/home?#/security_credentials)
    - Run `aws configure` and follow the prompts
- Deploy using [Terraform](https://www.terraform.io/)
    - `terraform init`
    - `terraform deploy`

## Tear Down

When you are ready to tear down the resources, use

```bash
terraform destroy
```

## Available Resources

I only left the ALB exposed to the public, it is accessible [here](http://alb-fortune-app-1306645501.us-east-1.elb.amazonaws.com/)

### Paths

- `/`
    - This is the base web app page
- `/fortune`
    - This is the direct call to the lambda

## My Plan

When I first read over the exercises, I first started to write all of the infrastructure in the AWS CDK. My primary experience with IoC is utilizing CloudFormation, which - for me - takes more time to write than what would be necessary for these exercises. I did not have a lot of experience with the CDK and after some experimenting, decided to switch to Terraform. I have wanted to use Terraform for a long time, but it is not approved for use at my organization. I really enjoyed the experience and appreciated the documentation and ease of use. There were some things that I had wanted to do, such as separate out `.tf` files for each of the exercises as well as a base file, but in my short attempts I was unsuccessful.

### Exercise One

Because a lot of the foundational resources are needed in order to get exercise one up and running, they were created first. I started with the plan for the virtual private cloud (VPC). I created three public and private subnets to launch instances into. Because I was using the module for a VPC, an internet gateway was created for me. In a similar fashion, because I specified `private_subnets` and set `enable_nat_gateway = true` my resources inside the private subnets would have access to the internet.

After creating the VPC and networking, I set out on the application load-balancer (ALB). At first, I created the target group and listener rules in the `module`. This worked when just using the `lambda` function, but I knew that I would have to come back and fix this later. I added a logging bucket on the ALB so that I could see requests that being made across the application. After creating the ALB and stubbing out some of its code, I started on the lambda. I did not edit the code in the lambda to make it perform any other functions, and just deployed it as is. When considering observability of the lambda, there are CloudWatch logs that happen for each invocation. These logs are stored in a unique log group per lambda. If we wanted to monitor the metrics in more of a dashboard, then we could add our lambda to a CloudWatch dashboard for the _fortune app_. I did notice that there were two different levels of logs in the lambda function: `error` and `info` but both are captured in the CloudFormation logs. For the lambda handler, terraform takes care of packaging the `.zip` file and provisioning it to the lambda. This could have also been done in an `S3` bucket if necessary, but it did not feel so here. The lambda as placed into the same VPC as the ALB, but in the private subnets. The associated security group (SG) only allows traffic from the security group associated with the ALB. This means that we don't have to worry about people hitting our lambda directly.

## Exercise Two

Exercise two built on the core of exercise one a lot, so there were not a lot of new AWS resources to provision. I stared by testing on an ephemeral EC2 instance to get a web server up and running. I really wanted to use a container based solution for the `web` portion of this exercise, but did not because the [README](exercise_2/README.md) explicitly stated an EC2 based instance. In order to serve the application, I used an [NGINX:ALPINE](https://hub.docker.com/_/nginx) docker image. The alpine version of the images produces smaller image sizes as well as end container sizes. Because bootstrapping the EC2 instances required no manual intervention, I used [docker-compose](https://docs.docker.com/compose/) to start the application with a custom service that was run on system start. I did try use the base `docker` service, but had an issue with the configuration. After the ephemeral EC2 instance was set up and running, I created an AMI from it. This is the AMI that I used for my auto-scaling group. Another way of configuring the bootstrapping would have been to utilize the EC2's [user data](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/user-data.html#user-data-console) functionality. The autoscaling group that I created will scale between one and five instances based on capacity. The instances in the autoscaling group were also placed into the same private subnets and SG as the lambda function, this helps to provide defense in depth. During this exercise, I also re-worked the lambda function to only operate on `/fortune` on the ALB, that way the default `/` url would be for the front end.

For monitoring the autoscaling group, CloudWatch again can be used. Because the application is basic (IE not running something like Express or Angular), we are not as easily able to use a logging utility like [Winston](https://github.com/winstonjs/winston) to transport application logs to CloudWatch or another service.

### Other Considerations

While programming these exercises, there are a few things that I would have done differently but did not due to the exercise requirements.

- Used API gateway over an ALB
    - I would have chosen this for better mapping between my APIs and the lambda that I was accessing
    - The API gateway is more redundant than a loadbalancer in the event that AWS were to have an issue
- Used [Copilot](https://aws.github.io/copilot-cli/) for the web application
    - I have the code necessary for this in the `copilot` branch
- This is not specific to the exercises, but more towards my experience with Terraform
    - I would have liked to have found a way to test the resources that I created. I know that there are health checks that can be preformed by the ALB, and for each target group. But If this code was being deployed in a pipeline, another way to test would have been nice. Terraform does provide the `plan` command and allow you to do sanity checks. But I was looking for something else.
    - Apologies if I committed something from Terraform that I was not supposed to, or omitted something that is considered best-practice.
