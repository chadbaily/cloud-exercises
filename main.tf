terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.53"
    }
  }
}

# Set up AWS defaults
provider "aws" {
  region = "us-east-1"
}

# Create a new VPC to hold the lambda + load balancer + asg
module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name                 = "fortune_vpc"
  cidr                 = "10.10.0.0/16"
  enable_dns_hostnames = true

  azs = ["us-east-1a", "us-east-1b", "us-east-1c"]

  # Set up Private Subnets for lambda to live in
  private_subnets = ["10.10.101.0/24", "10.10.102.0/24", "10.10.103.0/24"]

  # Add public_subnets for ALB to live in
  public_subnets = ["10.10.1.0/24", "10.10.2.0/24", "10.10.3.0/24"]

  # Set up NAT gateway for private subnets to allow lambda to make HTTP call
  enable_nat_gateway = true

  tags = {
    Name        = "fortune_vpc"
    Application = "fortune-app",
  }
}

module "alb_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = "alb-sg-fortune-app"
  description = "Security Group to be used with ALB"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["http-80-tcp", "all-icmp"]
  egress_rules        = ["all-all"]
  tags = {
    Name        = "asg_lambda_sg"
    Application = "fortune-app",
  }
}

module "asg_lambda_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = "asg-sg-fortune-app"
  description = "Security group for lambda + auto-scaling group"
  vpc_id      = module.vpc.vpc_id

  computed_ingress_with_source_security_group_id = [
    {
      rule                     = "http-80-tcp"
      source_security_group_id = module.alb_sg.security_group_id
    }
  ]
  number_of_computed_ingress_with_source_security_group_id = 1

  egress_rules = ["all-all"]
  tags = {
    Name        = "asg_lambda_sg"
    Application = "fortune-app",
  }
}

# S3 Bucket to log access to the ALB
module "log_bucket" {
  source  = "terraform-aws-modules/s3-bucket/aws"
  version = "~> 2.6"

  bucket                         = "fortune-alb-logs"
  acl                            = "log-delivery-write"
  force_destroy                  = true
  attach_elb_log_delivery_policy = true

  tags = {
    Name        = "alb_access_log"
    Application = "fortune-app",
  }
}

module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 6.3"

  name               = "alb-fortune-app"
  load_balancer_type = "application"

  vpc_id          = module.vpc.vpc_id
  security_groups = [module.alb_sg.security_group_id]
  subnets         = module.vpc.public_subnets

  depends_on = [
    module.log_bucket
  ]

  # Ideally you would make the bucket name dynamic, but there is currently a known bug
  # preventing this: https://github.com/hashicorp/terraform-provider-aws/issues/16674
  # access_logs = {
  #   bucket = module.log_bucket.s3_bucket_id
  # }
  access_logs = {
    bucket = "fortune-alb-logs"
  }

  # Set the ALB to forward to the lambda and prepare for the asg
  target_groups = [
    {
      name                               = "lambda-target-group"
      target_type                        = "lambda"
      lambda_multi_value_headers_enabled = true
      targets = {
        fortune_lambda = {
          target_id = module.lambda_function.lambda_function_arn
        }
      }
    },
    {
      name             = "asg-target-group"
      backend_protocol = "HTTP"
      backend_port     = 80
      target_type      = "instance"
    },
  ]


  tags = {
    Name        = "fortune-alb"
    Application = "fortune-app",
  }

}

# Set up a listener for the lambda
resource "aws_lb_listener" "fortune_listener" {
  load_balancer_arn = module.alb.lb_arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = module.alb.target_group_arns[0]
  }
  tags = {
    Name        = "fortune_app_listener"
    Application = "fortune-app",
  }
}

# Set up the listener rule for the lambda on /fortune
resource "aws_lb_listener_rule" "fortune_lambda" {
  # It is the first listener in the list
  listener_arn = aws_lb_listener.fortune_listener.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = module.alb.target_group_arns[0]
  }

  condition {
    path_pattern {
      values = ["/fortune/*"]
    }
  }

  tags = {
    Name        = "fortune_lambda_listener_rule"
    Application = "fortune-app",
  }
}

# Set up the listener rule for the asg on /fortune
resource "aws_lb_listener_rule" "fortune_asg" {
  # It is the first listener in the list
  listener_arn = aws_lb_listener.fortune_listener.arn
  priority     = 99

  action {
    type             = "forward"
    target_group_arn = module.alb.target_group_arns[1]
  }

  condition {
    path_pattern {
      values = ["/"]
    }
  }

  tags = {
    Name        = "fortune_asg_listener_rule"
    Application = "fortune-app",
  }
}

# https://registry.terraform.io/modules/terraform-aws-modules/lambda/aws/latest
# Python 3.7 was used because that is what I had installed on my computer and had issues updating to 3.8 with homebrew...
module "lambda_function" {
  source = "terraform-aws-modules/lambda/aws"

  function_name = "fortune-app-lambda"
  description   = "Lambda function to get your fortune"
  handler       = "app.lambda_handler"
  runtime       = "python3.7"
  publish       = true

  source_path = "exercise_1/fortune_handler"
  # Set up the VPC configuration
  vpc_subnet_ids = module.vpc.private_subnets

  # Because we are making this public, we are not concered with the IPs that will be accessing the function
  vpc_security_group_ids = [module.asg_lambda_sg.security_group_id]
  attach_network_policy  = true

  # Allow the ALB to trigger the lambda
  allowed_triggers = {
    AllowExecutionFromELB = {
      service    = "elasticloadbalancing"
      source_arn = module.alb.target_group_arns[0]
    }
  }

  tags = {
    Name        = "fortune_lambda",
    Application = "fortune-app",
  }

}


# Add EC2 autoscaling
module "asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 4.0"

  # Autoscaling group name
  name = "fortune_asg"

  vpc_zone_identifier = module.vpc.private_subnets
  min_size            = 0
  max_size            = 5
  desired_capacity    = 1

  # Launch template
  use_lt    = true
  create_lt = true

  image_id      = "ami-023fa6e21ef949c28"
  instance_type = "t2.micro"

  security_groups   = [module.asg_lambda_sg.security_group_id]
  target_group_arns = [module.alb.target_group_arns[1]]

  enable_monitoring = true

  tags = [{
    Application = "fortune-app",
  }]
}

# Add auto scale policy
resource "aws_autoscaling_policy" "asg_capicity_scaling" {
  name                   = "capicity-scaling"
  scaling_adjustment     = 4
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = module.asg.autoscaling_group_name
}
