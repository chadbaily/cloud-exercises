# Outputs for ease of use
output "alb_dns_name" {
  value       = module.alb.lb_dns_name
  description = "DNS name for the ALB which points to the fortune Lambda"
}
